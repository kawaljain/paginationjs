# paginationJs

Client Side Pagination is useful when we have display small no of records (less than 1000).Server-side pagination is needed when you have to display hundreds of records. You may fetch results from the database using an offset and loading a single page for each HTTP request.
For smaller ones – less than 1000 – and when the query is optimized, you can use client-side navigation.

## Advantage
-	Allow for more interactivity by immediately responding to users' actions
-	Execute quickly because they don't require a trip to the server
-	May improve the usability of Web sites for users whose browsers support scripts

## List of Function

-	totalPage()
-	nextPage(page)
-	previousPage(page)
-	paginationInit(pageNo=1 )
-	paginationDisplay

### Prerequisites

First, we need to include the jquery file. 
After that we need to include the pagination.js file.

### Step 1

Create the object of helper Class.

```
var pagination = new Pagination({
            'itemPerPage':5,
            'data':data,
        });
```


In above example, we Initialization the pagination class. It accepting two parameter:-

-	itemPerPage :– define how many list of data you want to show per page
-	data :- array of objects contain the data. 

### Step 2

Now start using function using pagination variable

```
pagination.paginationInit()
```


## Description

### paginationInit()

This function will return the html code of pagination based data which we send at the time of Initialization .

```
$('#projectPagination').html(pagination.paginationInit());
```

### paginationDisplay(page)
        

Return the list of data based on the page number. This function one argument
	- page : Page number 

```
	$( "#tableSturture" ).tmpl(pagination.paginationDisplay(1)).appendTo( "#template");
```

## Plugins

* Bootstrap 3 - Document Link - (https://getbootstrap.com/docs/3.3/) - The web framework used
* jquery-tmpl -Github Link - (https://github.com/BorisMoore/jquery-tmpl) - Dependancy

## Authors

* **Kawal Jain** - *Initial work* - [website]( http://kawaljain.com/)

