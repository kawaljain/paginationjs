
/*
 * 
 * Author: Kawal Jain
 * Author Email: kawaljain03@gmail.com
 * Author Website: http://kawaljain.com/ * 
 */

class Pagination{
    
    constructor(data) {
		this.totalRecords(data.data);
        this.itemPerPages(data.itemPerPage);
	}
	totalRecords(data){
		this.totalRecord=data;
	}
	itemPerPages(data){
		this.itemPerPage = data;
	}
    totalPage(){
		return Math.ceil(this.totalRecord.length / this.itemPerPage);
	}
	nextPage(page){
		return page+1;
	}
	previousPage(page){
		return page-1;
	}
	paginationInit(pageNo=1 ){
		let selectedpage=pageNo;
		let i,html,pagerCount,previouspage,nextpage;
        pagerCount = this.totalPage();
        previouspage=this.previousPage(selectedpage);
		nextpage=this.nextPage(selectedpage);		
		if(selectedpage==1){
            html='<li class="page-item disabled"><a class="page-link projectpagi disabled" >Previous</a></li> ';            
        }else{
            html='<li class="page-item " ><a class="page-link projectpagi" href="#" value="'+previouspage+'">Previous</a></li>';
        }
        for(i=1;i<=pagerCount;i++){
            if(i==selectedpage){
                html+='<li class="page-item active"><a class="page-link projectpagi" href="#" value="'+i+'">'+i+'</a></li>';
            }else{
                html+='<li class="page-item"><a class="page-link projectpagi" href="javscript(void)" value="'+i+'">'+i+'</a></li>';
            }

        }
        if(selectedpage==pagerCount){
            html+='<li class="page-item disabled"><a href="return false" class="page-link disabled projectpagi" >Next</a></li>';
        }else{
            html+='<li class="page-item"><a class="page-link projectpagi" value="'+nextpage+'">Next</a></li>';
		}
		return html;
	}
	paginationDisplay(page){
		let index,datas;
		datas = this.totalRecord;
		index = (page-1)* this.itemPerPage;
		datas = datas.slice(index,index+this.itemPerPage);
		return datas;
	}
}
